#!/bin/bash

versioneAsterisk=15.4.0
opzioniAsterisk=$(pwd)/menuselect.makeopts

if [ ! -f ${opzioniAsterisk} ]
then
    echo "Non trovo il file con le opzioni di compilazione di asterisk, non posso continuare"
    exit 1
fi

function installaPacchetti()
{
    array=("$@")
    daInstallare=()

    for pkg in "${array[@]}"
    do
        rpm -q $pkg >/dev/null
        if [ $? -ne 0 ]
        then
            echo "Preparando l’installazione del pacchetto $pkg"
            daInstallare+=($pkg)
        fi
    done

    if [ "x${daInstallare}x" != "xx" ]
    then
        echo "Installando ${daInstallare[@]}"
        yum -y install ${daInstallare[@]} >/dev/null || return 1
    fi
}

function scaricaAsterisk()
{
    cd /usr/src
    rm -f dahdi-linux-complete-current.tar.gz libpri-current.tar.gz asterisk.tar.gz jansson.tar.gz
    wget -qc http://downloads.asterisk.org/pub/telephony/dahdi-linux-complete/dahdi-linux-complete-current.tar.gz
    wget -qc http://downloads.asterisk.org/pub/telephony/libpri/libpri-current.tar.gz || return 1
    wget -qc -O asterisk.tar.gz http://downloads.asterisk.org/pub/telephony/asterisk/releases/asterisk-${versioneAsterisk}.tar.gz || return 1
    wget -qc -O jansson.tar.gz https://github.com/akheron/jansson/archive/v2.10.tar.gz || return 1
}

function compilaDahdi()
{
    cd /usr/src
    tar xf dahdi-linux-complete-current.tar.gz --skip-old-files
    cd dahdi-linux-complete-*    
    make all
    make install
    make config
}

function compilaLibpri()
{
    cd /usr/src
    tar xfz libpri-current.tar.gz --skip-old-files
    cd /usr/src/libpri-*
    make
    make install
}

function compilaJansson()
{
    cd /usr/src
    tar xf jansson.tar.gz --skip-old-files
    cd jansson-*
    autoreconf -i
    ./configure --libdir=/usr/lib64
    make
    make install
}

function compilaAsterisk()
{
    
    cd /usr/src
    tar xf asterisk.tar.gz --skip-old-files
    cd asterisk-*
    ./configure --libdir=/usr/lib64 --with-pjproject-bundled
    contrib/scripts/get_mp3_source.sh
    cp -a ${opzioniAsterisk} .
    make
    make install
    make config
    make samples
    ldconfig
    service asterisk restart
    chown asterisk:asterisk /var/run/asterisk
    chown -R asterisk:asterisk /etc/asterisk
    chown -R asterisk:asterisk /var/{lib,log,spool}/asterisk
    chown -R asterisk:asterisk /usr/lib64/asterisk
}

#Attualizza ed installa alcuni pacchetti base
echo "Installando pacchetti..."
yum -q -y upgrade >/dev/null || exit 1
yum -q -y groupinstall core base "Development Tools" 2>/dev/null || exit 1
installaPacchetti epel-release lynx tftp-server unixODBC mysql-connector-odbc mariadb \
    ncurses-devel sendmail sendmail-cf sox newt-devel libxml2-devel libtiff-devel \
    audiofile-devel gtk2-devel subversion kernel-devel kernel-devel-`uname -r` git crontabs cronie \
    cronie-anacron wget uuid-devel sqlite-devel net-tools gnutls-devel python-devel texinfo \
    libuuid-devel vim-enhanced alsa-lib-devel binutils-devel bluez-libs-devel codec2-devel \
    corosynclib-devel fftw-devel freetds-devel gmime-devel graphviz gsm-devel \
    jack-audio-connection-kit-devel jansson-devel libcurl-devel libedit-devel libical-devel \
    libogg-devel libsndfile-devel libsrtp-devel libtool-ltdl-devel libvorbis-devel \
    libxslt-devel lua-devel mariadb-devel neon-devel net-snmp-devel openldap-devel \
    openssl-devel popt-devel portaudio-devel postgresql-devel radcli-devel \
    spandsp-devel speex-devel unbound-devel unixODBC-devel uriparser-devel \
    uw-imap-devel xmlstarlet opus-devel

if [ $? -ne 0 ]; then
    echo "Errore installando i pacchetti, per favore controlla il collegamento ad internet e riprova"
    exit 1
fi

#Disattiva Selinux
echo "Disattivando selinux..."
if [ $(getenforce) != "Permissive" ]; then 
    setenforce 0
    sed -i 's/\(^SELINUX=\).*/\SELINUX=disabled/' /etc/sysconfig/selinux
fi

echo "Aggiustando la time zone..."
timedatectl set-timezone Europe/Rome

#Crea l utente asterisk
echo "Creando utente asterisk..."
getent passwd asterisk >/dev/null || \
adduser asterisk -m -c "Asterisk User"

echo "Scaricando il source code di asterisk..."
scaricaAsterisk

if [ $? -ne 0 ]; then
    echo "Errore scaricando il source code di asterisk, per favore controlla il collegamento ad internet e riprova"
    exit 1
fi

echo "Compilando DAHDI..."
compilaDahdi &>/dev/null

if [ $? -ne 0 ]; then
    echo "Errore compilando DAHDI. Vai a /usr/src/dahdi-linux-complete-* e prova a compilare manualmente con make all"
    exit 1
fi

echo "Compilando libpri..."
compilaLibpri >/dev/null

if [ $? -ne 0 ]; then
    echo "Errore compilando libpri. Vai a /usr/src/libpri-* e prova a compilare manualmente con make"
    exit 1
fi

echo "Compilando jansson..."
compilaJansson >/dev/null

if [ $? -ne 0 ]; then
    echo "Errore compilando jansson. Vai a /usr/src/jansson-* e prova a compilare manualmente con make"
    exit 1
fi

echo "Compilando asterisk..."
compilaAsterisk >/dev/null

if [ $? -ne 0 ]; then
    echo "Errore compilando asterisk. Vai a /usr/src/asterisk-* e prova a compilare manualmente con make"
    exit 1
fi

echo "Installando il codec g.729..."
cd /usr/lib64/asterisk/modules
wget -q -O codec_g729.so http://asterisk.hosting.lv/bin/codec_g729-ast150-gcc4-glibc-x86_64-core2-sse4.so
wget -q -O codec_g723.so http://asterisk.hosting.lv/bin/codec_g723-ast150-gcc4-glibc-x86_64-core2-sse4.so
asterisk -rx "module reload"

#Tutto a posto, cancelliamo le cartelle di compilazione ed usciamo
find /usr/src -mindepth 1 -maxdepth 1 -type d \
    \( -name dahdi-linux-complete-\* -o -name libpri-\* -o -name jansson-\* -o -name asterisk-\* \) \
    -exec rm -rf {} \;
echo "Installazione di asterisk completa!"
