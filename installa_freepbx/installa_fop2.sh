#!/bin/bash

if [ ! -f /etc/freepbx.conf ]
then
	echo "Per una installazione corretta di FOP2 devi installare prima il FreePBX. esegui il comando ./installa_freepbx.sh e dopo ./installa_fop2.sh"
	exit 1
elif [ -d /var/www/html/fop2 ]
then
	echo "Fop2 è già installato"
	exit 1
else
	wget -O - http://download.fop2.com/install_fop2.sh | bash
	echo "FOP2 Installato. Collegati almento una volta alla sua interfaccia di amministrazione su http://<ip del server>/fop2/admin"
	echo "con il nome e la password dell’ amministratore di freepbx."
	echo "Poi potrai usare Fop2 su http://<ip del server>/fop2".
	echo "Per usare fop2 fuori dalla vpn dovrai aprire la porta 4445/tcp su shorewall"
fi
