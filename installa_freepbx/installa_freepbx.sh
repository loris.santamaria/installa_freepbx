#!/bin/bash

moduliFreepbx="ringgroups timeconditions ivr queues paging customcontexts backup announcement endpointman calendar customappsreg recordings paging ucp webrtc"

servizioSystemd=$(pwd)/freepbx.service

if [ ! -f ${servizioSystemd} ]
then
    echo "Non trovo l’unit file di systemd"
    exit 1
fi

function installaPacchetti()
{
    array=("$@")
    daInstallare=()

    for pkg in "${array[@]}"
    do
        rpm -q $pkg >/dev/null
        if [ $? -ne 0 ]
        then
            echo "Preparando l’installazione del pacchetto $pkg"
            daInstallare+=($pkg)
        fi
    done

    if [ "x${daInstallare}x" != "xx" ]
    then
        echo "Installando ${daInstallare[@]}"
        yum -y install ${daInstallare[@]} >/dev/null || return 1
    fi
}

function installaNodejs()
{
    curl -sL https://rpm.nodesource.com/setup_8.x | bash -
    yum install -y nodejs
}

#Attualizza ed installa alcuni pacchetti base
echo "Installando pacchetti..."
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm >/dev/null
installaPacchetti mariadb-server mariadb httpd subversion git crontabs cronie \
    cronie-anacron wget php56w php56w-pdo php56w-mysql php56w-mbstring php56w-pear \
    php56w-process php56w-xml php56w-opcache php56w-ldap php56w-intl php56w-soap incron

if [ $? -ne 0 ]; then
    echo "Errore installando i pacchetti, per favore controlla il collegamento ad internet e riprova"
    exit 1
fi

echo "Installando nodejs..."
installaNodejs >/dev/null

echo "Configurando MariaDB..."
systemctl enable mariadb.service
systemctl start mariadb
mysql -uroot <<-EOF
    DELETE FROM mysql.user WHERE User='';
    DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
    DROP DATABASE IF EXISTS test;
    DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
    FLUSH PRIVILEGES;
EOF


echo "Configurando le estensioni PHP Pear..."
pear -q install Console_Getopt >/dev/null

echo "Disattiva asterisk, freepbx lo attiverà posteriormente..."
chkconfig asterisk off
service asterisk stop
rm -rf /etc/asterisk/*

echo "Configurando Apache..."
sed -i 's/\(^upload_max_filesize = \).*/\120M/' /etc/php.ini
sed -i 's/^\(User\|Group\).*/\1 asterisk/' /etc/httpd/conf/httpd.conf
sed -i 's/AllowOverride None/AllowOverride All/' /etc/httpd/conf/httpd.conf
systemctl enable httpd.service
systemctl restart httpd.service

echo "Scaricando il source code di FreePBX..."
cd /usr/src
wget -qc http://mirror.freepbx.org/modules/packages/freepbx/freepbx-14.0-latest.tgz

if [ $? -ne 0 ]; then
    echo "Errore scaricando il source code di freepbx, per favore controlla il collegamento ad internet e riprova"
    exit 1
fi

echo "Installando FreePBX..."
tar xf freepbx-14.0-latest.tgz --skip-old-files
cd freepbx
./start_asterisk start
./install -qnf >/dev/null 

if [ $? -ne 0 ]; then
    echo "Errore installando FreePBX. Vai a /usr/src/freepbx ed esegui l’istallazione a mano col comando ./install -nf"
    exit 1
fi

if [ ! -f /etc/systemd/system/freepbx.service ]
then
    echo "Installando la unit di systemd per FreePBX..."
    cp ${servizioSystemd} /etc/systemd/system/
    systemctl daemon-reload
    systemctl enable freepbx.service
fi

systemctl restart freepbx.service

echo "Attualizzando i moduli base di FreePBX..."
fwconsole -q ma upgradeall
fwconsole reload

echo "Installando i moduli $moduliFreepbx"
fwconsole -q ma download $moduliFreepbx
fwconsole -q ma install $moduliFreepbx

fwconsole reload
chown asterisk:asterisk /var/lib/asterisk/astdb.sqlite3
fwconsole restart
#Tutto a posto, cancelliamo le cartelle di compilazione 
rm -rf /usr/src/freepbx
echo "
Installazione completa!

Per un corretto funzionamento dell User Control Panel de del telefono WebRTC
si raccomanda di aggiungere questo PBX al DNS e di richiedere 
un certificato di letsencrypt usando il Certificate Manager di FreePBX
"
