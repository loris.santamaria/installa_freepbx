#!/bin/bash

function installaPacchetti()
{
    array=("$@")
    daInstallare=()

    for pkg in "${array[@]}"
    do
        rpm -q $pkg >/dev/null
        if [ $? -ne 0 ]
        then
            echo "Preparando l’installazione del pacchetto $pkg"
            daInstallare+=($pkg)
        fi
    done

    if [ "x${daInstallare}x" != "xx" ]
    then
        echo "Installando ${daInstallare[@]}"
        yum -y install ${daInstallare[@]} >/dev/null || return 1
    fi
}

#Attualizza ed installa alcuni pacchetti base
echo "Installando pacchetti..."
installaPacchetti shorewall fail2ban fail2ban-shorewall xl2tpd libreswan openvpn


if [ $? -ne 0 ]; then
    echo "Errore installando i pacchetti, per favore controlla il collegamento ad internet e riprova"
    exit 1
fi

#Apre le porte del firewall
echo "Configurando shorewall..."
systemctl stop firewalld
systemctl disable firewalld

sed -i 's/STARTUP_ENABLED=No/STARTUP_ENABLED=Yes/' /etc/shorewall/shorewall.conf

cat >/etc/shorewall/zones <<EOF
#ZONE		TYPE		OPTIONS		IN_OPTIONS	OUT_OPTIONS
fw		firewall
vpn		ipv4
net		ipv4
EOF

cat >/etc/shorewall/interfaces <<EOF
?FORMAT 2
###############################################################################
#ZONE		INTERFACE		OPTIONS
vpn		tun+
vpn		ppp+
net		eth0			optional
net		ens192			optional
EOF

cat >/etc/shorewall/policy <<EOF
#SOURCE		DEST		POLICY	LOGLEVEL	RATE	CONNLIMIT
fw		all		ACCEPT
vpn		all		ACCEPT
net		all		DROP
EOF

cat >/etc/shorewall/rules <<EOF
#ACTION		SOURCE		DEST		PROTO	DPORT	SPORT	ORIGDEST	RATE	USER	MARK	CONNLIMIT	TIME	HEADERS	SWITCH	HELPER

?SECTION ALL
?SECTION ESTABLISHED
?SECTION RELATED
?SECTION INVALID
?SECTION UNTRACKED
?SECTION NEW

ACCEPT		net		fw		tcp	22,65200	#ssh
ACCEPT		net		fw		tcp	5061,8089	#Sips,Wss
ACCEPT		net		fw		udp	10000:20000	#rtp
EOF

cat >/etc/shorewall/tunnels <<EOF
#TYPE			ZONE		GATEWAY			GATEWAY_ZONE
openvpnserver:tcp	net		0.0.0.0/0
l2tp			net		0.0.0.0/0
EOF

cat >/etc/shorewall/hosts <<EOF
#ZONE		HOSTS				OPTIONS
vpn             eth0:0.0.0.0/0                  ipsec
EOF

systemctl start shorewall
systemctl enable shorewall

echo "Installando server l2tp..."
cat >/etc/xl2tpd/xl2tpd.conf <<EOF
[global]
;listen-addr = 80.211.136.227

[lns default]
ip range = 10.0.255.128-10.0.255.254
local ip = 10.0.255.1
assign ip = yes
require chap = yes
refuse pap = yes
require authentication = yes
name = LinuxVPNserver
ppp debug = yes
pppoptfile = /etc/ppp/options.xl2tpd
length bit = yes
EOF

cat >/etc/ppp/options.xl2tpd <<EOF
ipcp-accept-local
ipcp-accept-remote
ms-dns  8.8.8.8
noccp
auth
idle 1800
mtu 1410
mru 1410
nodefaultroute
debug
proxyarp
connect-delay 5000
EOF

cat >/etc/ppp/chap-secrets <<EOF
# Utenti e password per il L2TP
# Si puo anche assegnare una ip fissa a un cliente
# Questa ip deve stare nel rango definito in /etc/xl2tpd/xl2tpd.conf
# client	server	secret			IP addresses
mikrotik1	*	"thooDa6woongachu"	10.0.255.128/25
mikrotik2	*	"ohteesh3wo2Ohphi"	10.0.255.127
EOF
chmod 600 /etc/ppp/chap-secrets

cat >/etc/ppp/ip-up.local <<EOF
#Qui si possono aggiungere le rotte
#alle reti Lan del cliente
#ip route add 10.0.51.0/24 via 10.0.255.127
EOF
chmod 755 /etc/ppp/ip-up.local

cat >/etc/ipsec.d/l2tpd.conf <<EOF
conn l2tp-psk
        authby=secret
        pfs=no
        auto=add
        rekey=no
        type=transport
        left=%defaultroute
        leftprotoport=udp/l2tp
        right=%any
	rightid=%any
        rightprotoport=udp/l2tp
	dpddelay=15
	dpdtimeout=30
	dpdaction=clear
conn l2tp-psk-nat
	also=l2tp-psk
	rightsubnet=vhost:%priv
EOF

cat >/etc/ipsec.d/l2tpd.secrets <<EOF
: PSK "nimaigaiDaewe5ah"
EOF
chmod 600 /etc/ipsec.d/l2tpd.secrets

systemctl enable ipsec xl2tpd
systemctl start ipsec xl2tpd

echo "Installando server openvpn..."

cat > /etc/openvpn/server.conf <<EOF
port 1194
proto tcp-server
dev tap

ca ca.crt
cert pbx.server.it.crt
key pbx.server.it.key
tls-server

dh dh2048.pem

topology subnet
server 10.0.15.0 255.255.255.0
ifconfig-pool-persist ipp.txt

keepalive 10 120
cipher AES-256-CBC
persist-key
persist-tun
status openvpn-status.log
verb 3
EOF

openssl dhparam -out /etc/openvpn/dh2048.pem 2048
systemctl enable openvpn@server

echo "Installando fail2ban..."

cat >/etc/fail2ban/jail.local <<EOF
[DEFAULT]
ignoreip = 127.0.0.1
bantime  = 1800
findtime  = 600
maxretry = 5
backend = auto
banaction = shorewall
action_ = %(banaction)s[name=%(__name__)s, bantime="%(bantime)s", port="%(port)s", protocol="%(protocol)s", chain="%(chain)s"]

[asterisk]
enabled  = true
filter = asterisk
logpath  = /var/log/asterisk/full
maxretry = 5
bantime = 1800

[pbx-gui]
enabled  = true
filter   = freepbx
logpath  = /var/log/asterisk/freepbx.log
maxretry = 5
bantime = 1800

[ssh-iptables]
enabled  = true
port     = 22,65200
filter   = sshd
logpath  = /var/log/secure
maxretry = 3

[apache-tcpwrapper]
enabled  = true
filter   = apache-auth
logpath  = /var/log/httpd/error_log
maxretry = 3

[apache-badbots]
enabled  = true
filter   = apache-badbots
logpath  = /var/log/httpd/*access_log
bantime  = 1800
maxretry = 1
EOF

cat >/etc/fail2ban/filter.d/asterisk.conf <<EOF
[INCLUDES]

# Read common prefixes. If any customizations available -- read them from
# common.local
before = common.conf

[Definition]

_daemon = asterisk

__pid_re = (?:\[\d+\])

iso8601 = \d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d+[+-]\d{4}

# All Asterisk log messages begin like this:
log_prefix= (?:NOTICE|SECURITY)%(__pid_re)s:?(?:\[C-[\da-f]*\])? \S+:\d*( in \w+:)?

failregex = ^(%(__prefix_line)s|\[\]\s*)%(log_prefix)s Registration from '[^']*' failed for '<HOST>(:\d+)?' - (Wrong password|Username/auth name mismatch|No matching peer found|Not a local domain|Device does not match ACL|Peer is not supposed to register|ACL error \(permit/deny\)|Not a local domain)$
            ^(%(__prefix_line)s|\[\]\s*)%(log_prefix)s Call from '[^']*' \(<HOST>:\d+\) to extension '[^']*' rejected because extension not found in context
            ^(%(__prefix_line)s|\[\]\s*)%(log_prefix)s Host <HOST> failed to authenticate as '[^']*'$
            ^(%(__prefix_line)s|\[\]\s*)%(log_prefix)s No registration for peer '[^']*' \(from <HOST>\)$
            ^(%(__prefix_line)s|\[\]\s*)%(log_prefix)s Host <HOST> failed MD5 authentication for '[^']*' \([^)]+\)$
            ^(%(__prefix_line)s|\[\]\s*)%(log_prefix)s Failed to authenticate (user|device) [^@]+@<HOST>\S*$
            ^(%(__prefix_line)s|\[\]\s*)%(log_prefix)s hacking attempt detected '<HOST>'$
            ^(%(__prefix_line)s|\[\]\s*)%(log_prefix)s <HOST> tried to authenticate with nonexistent user.+$
            ^(%(__prefix_line)s|\[\]\s*)%(log_prefix)s <HOST> failed to authenticate as.+$
            ^(%(__prefix_line)s|\[\]\s*)%(log_prefix)s Request from '[^']*' failed for '<HOST>:\d+' .+ No matching endpoint found$
            ^(%(__prefix_line)s|\[\]\s*)%(log_prefix)s SecurityEvent="(FailedACL|InvalidAccountID|ChallengeResponseFailed|InvalidPassword)",EventTV="([\d-]+|%(iso8601)s)",Severity="[\w]+",Service="[\w]+",EventVersion="\d+",AccountID="(\d*|<unknown>)",SessionID=".+",LocalAddress="IPV[46]/(UDP|TCP|WS|WSS)/[\da-fA-F:.]+/\d+",RemoteAddress="IPV[46]/(UDP|TCP|WS|WSS)/<HOST>/\d+"(,Challenge="[\w/]+")?(,ReceivedChallenge="\w+")?(,Response="\w+",ExpectedResponse="\w*")?(,ReceivedHash="[\da-f]+")?(,ACLName="\w+")?$
# These WARNINGS do not have a file attribute, as they're generated dynamicly
            ^(%(__prefix_line)s|\[\]\s*WARNING%(__pid_re)s:?(?:\[C-[\da-f]*\])? )[^:]+: Friendly Scanner from <HOST>$
            ^(%(__prefix_line)s|\[\]\s*WARNING%(__pid_re)s:?(?:\[C-[\da-f]*\])? )Ext\. s: "Rejecting unknown SIP connection from <HOST>"$

ignoreregex =
EOF

cat >/etc/fail2ban/filter.d/freepbx.conf <<EOF
[INCLUDES]

# Read common prefixes. If any customizations available -- read them from
# common.local
#before = common.conf


[Definition]

#_daemon = freepbx

# Option:  failregex
# Notes.:  regex to match the password failures messages in the logfile. The
#          host must be matched by a group named "host". The tag "<HOST>" can
#          be used for standard IP/hostname matching and is only an alias for
#          (?:::f{4,6}:)?(?P<host>[\w\-.^_]+)
# Values:  TEXT
#
failregex = Authentication failure for .* from <HOST>

# Option:  ignoreregex
# Notes.:  regex to ignore. If this regex matches, the line is ignored.
# Values:  TEXT
#
ignoreregex =
EOF

systemctl enable fail2ban
systemctl start fail2ban

echo "
Installazione completa!
Ricorda personalizzare la configuraziones del vpn:

1) Modifica la password in /etc/ppp/chap-secrets
2) Modifica la password in /etc/ipsec.d/l2tpd.secrets
3) Modifica la rotta alla lan privata in /etc/ppp/ip-up.local
4) Riavvia la vpn con il comando systemctl restart ipsec xl2tpd
"

